import { useEffect, useState } from 'react';

import axios from 'axios';

const useGetProducts = (API) => {
    const [products, setProducts] = useState([])

    useEffect(async () => {
        const Response = await axios.get(API)
        // console.log("Response.data", Response.data);
        setProducts(Response.data)
    }, [])

    return products;

}

export default useGetProducts; 