import { useState } from 'react';


const initialState = {
    cart: [],
}

const useInitialState = () => {
    const [state, setState] = useState(initialState);

    const addToCart = (payload) => {
        let listCart = [...state.cart];
        const p = listCart.map(row => row.id).indexOf(payload.id);
        if (p !== -1) {
            const pr = listCart.find(row => row.id === payload.id);
            listCart.splice(p, 1, { ...pr, amount: pr.amount + 1 });
        }
        else { listCart.push({ ...payload, amount: 1 }) }
        setState({
            ...state,
            cart: listCart
        })
    };

    const removeFromCart = (payload) => {
        setState({
            ...state,
            cart: state.cart.filter(item => item.id != payload.id)
        })
    }

    return {
        state,
        addToCart,
        removeFromCart
    }
}

export default useInitialState;