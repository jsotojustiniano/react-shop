import React, { useContext } from 'react';

import '@styles/OrderItem.scss';
import closeIcon from '@icons/icon_close.png'
import AppContext from '../context/AppContext';

const OrderItem = ({ product }) => {

    const { removeFromCart } = useContext(AppContext)

    const handleRemove = product => {
        removeFromCart(product)
    }

    return (
        <div className="OrderItem">
            <figure>
                <img src={product.images[0]} alt={product.title} />
            </figure>
            <span>{product.title}</span>
            <span>{product.amount}</span>
            <span>${product.price}</span>
            <span><img src={closeIcon} alt="close" onClick={() => handleRemove(product)} /></span>
        </div>
    );
}

export default OrderItem;